import sys, os, re, operator, math
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

class Player:
	# constructor:
	def __init__(self, name):
		self.name = name
		self.atBats = 0
 		self.hits = 0

	@staticmethod
	def get_definition(self):
		return "Player is a player on the Cardinals"

	def addAtBats(self, newAtBats):
		self.atBats = self.atBats + newAtBats

	def addHits(self, newHits):
		self.hits = self.hits + newHits

	def getHits(self):
		return self.hits

	def getBats(self):
		return self.atBats

	def batAvg(self):
		if self.hits > 0:
			return self.hits/float(self.atBats)
		else:
			return "not a number"

	def playerName(self):
		return self.name

playerNames={}
playerAverages={}
name_regex = re.compile(r"^[A-Z][a-z]+ [A-z][a-z]+")
stat_regex = re.compile(r"\d+")
f = open(filename)
for line in f:
	line = line.rstrip()
	match = name_regex.findall(line)
	stats = stat_regex.findall(line)
	if len(match) > 0:
		if playerNames.get(match[0]) is not None:
			playerNames[match[0]].addAtBats(int(stats[0]))
			playerNames[match[0]].addHits(int(stats[1]))
		else:
			playerNames[match[0]] = Player(match[0])
			playerNames[match[0]].addAtBats(int(stats[0]))
			playerNames[match[0]].addHits(int(stats[1]))
f.close()

for key, value in playerNames.iteritems():
	playerAverages[key]= round(value.batAvg(), 3)

sorted_avg = sorted(playerAverages.items(), key=operator.itemgetter(1), reverse=True)

for name, avg in sorted_avg:
	print "%s : %.3f" %(name, avg)

	









	<?php 
		require 'database.php';
		session_start();
	?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Login Page</title>
</head>
<body>
	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		Username: <input type="text" name="username"><br>
		Password: <input autocomplete="off" type="text" name="password"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "log" value="Login!" />
	</form>

	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		New Username: <input type="text" name="newUsername"><br>
		New Password: <input autocomplete="off" type="text" name="newPassword"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "reg" value="Register!" />
	</form>

	<form enctype="multipart/form-data" action = guestNewsPage.php>
		<input type ="submit" name = "guest" value="Guest Sign On">
	</form>

	<?php
		if (isset($_POST['reg'])) {//Registration code
			$newUser = $_POST['newUsername'];
			$newPass = crypt($_POST['newPassword'], "j?!!ododo9*oss");

			$stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('ss', $newUser, $newPass);
			$stmt->execute();
			$stmt->close();

			$stmt = $mysqli->prepare("select id, username from users where username=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('s', $newUser);
			$stmt->execute();
			$stmt->bind_result($id, $user_id);
			while($stmt->fetch()){
				printf("",
					htmlspecialchars($id),
					htmlspecialchars($user_id)
			);
			}
			$_SESSION['id']= $id;
			if(!$_SESSION['token']){
				$_SESSION['token'] = substr(md5(rand()), 0, 10);
			}

			header('Location: /~NikolaiLaba/newsPage.php');
		}
		if (isset($_POST['log'])){//login code
			$stmt = $mysqli->prepare("select id, username, password from users where username=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$username = (!empty($_POST['username']) ? $_POST['username'] : null);
			$password = (!empty($_POST['password']) ? $_POST['password'] : null);
			$stmt->bind_param('s', $_POST['username']);
			$stmt->execute();
			$stmt->bind_result($id, $user_id, $pwd_hash);

			while($stmt->fetch()){
				printf("",
					htmlspecialchars($id),
					htmlspecialchars($user_id),
					htmlspecialchars($pwd_hash)
			);
			}
			if(crypt($password, "j?!!ododo9*oss")===$pwd_hash){
				// Login succeeded!
				$_SESSION['user_id'] = $user_id;
				$_SESSION['id'] = $id;
				if(!$_SESSION['token']){
					$_SESSION['token'] = substr(md5(rand()), 0, 10);
				}
				header('Location: /~NikolaiLaba/newsPage.php');
				// Redirect to your target page
			}
			else{
				exit();
			}
		}
		if (isset($_GET['guest'])){//guest redirection
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
		}
	?>
</body>
</html>
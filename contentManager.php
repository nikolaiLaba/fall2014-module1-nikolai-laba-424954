<?php 
require 'database.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Content Manager</title>
</head>
<body>	
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		Story Number: <input type="text" name="id"><br>
		Edited Story: <input type="text" name="editStory"><br>
		Story Link: <input type="text" name="editPage"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "sub" value="Submit Story Update!" />
	</form>
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		Story Number to Delete: <input type="text" name="storyNumDel">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "del" value="Delete!" />
	</form>
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		Comment Number: <input type="text" name="comNum"><br>
		Edited Comment: <input type="text" name="editCom"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "com" value="Submit Comment Update!" />
	</form>
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		Comment Number to Delete: <input type="text" name="comNumDel">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "del2" value="Delete!" />
	</form>
	<form enctype="multipart/form-data" action = newsPage.php method="POST">
		<input type="submit" name = "ref" value="News Page!" />
	</form>
	<form enctype="multipart/form-data" action = comments.php method="POST">
		<input type="submit" name = "com" value="To the Comments section!" />
	</form>
	<form enctype="multipart/form-data" action = userManager.php method="POST">
		<input type="submit" name = "usr" value="To User Management" />
	</form>
	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		<input type="submit" name = "log" value="Logout" />
	</form>
	Your Stories:
	<?php
		require 'database.php';
		if(!$_SESSION['token']){
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
		}	
		if (isset($_POST['sub'])) {//code to edit a story
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}	
			$id = (!empty($_POST['id']) ? $_POST['id'] : null);
			$editStory = (!empty($_POST['editStory']) ? $_POST['editStory'] : null);
			$newLink = (!empty($_POST['editPage']) ? $_POST['editPage'] : null);
			$verified = $mysqli->prepare("select user_id from stories where id=?");
			if(!$verified){ //if the stateement is not properly formatted
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$verified->bind_param('i', $id);
			$verified->execute();
			$verified->bind_result($user_id);
			while($verified->fetch()){
				printf("\t<li>%s",
					htmlspecialchars($user_id)
			);
			}
			$verified->close();
			if($user_id==$_SESSION['id']){ 
				$stmt = $mysqli->prepare("update stories set story=?, storyLink=? where id=?");
				if(!$stmt){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$stmt->bind_param('ssi', $editStory, $newLink, $id);
				$stmt->execute();
				$stmt->close();
			}
		}

		if (isset($_POST['del'])) {//delete a story
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}	
			$storyNumDel = (!empty($_POST['storyNumDel']) ? $_POST['storyNumDel'] : null); 
			$verified = $mysqli->prepare("select user_id from stories where id=?");
			if(!$verified){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$verified->bind_param('i', $storyNumDel);
			$verified->execute();
			$verified->bind_result($user_id);
			while($verified->fetch()){
				printf("\t<li>%s",
					htmlspecialchars($user_id)
			);
			}
			$verified->close();
			if($user_id==$_SESSION['id']){
				$comDelete = $mysqli->prepare("delete from comments where story_id=?");
				if(!$comDelete){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$comDelete->bind_param('i', $storyNumDel);
				$comDelete->execute();
				$comDelete->close();
				$verified = $mysqli->prepare("delete from stories where id=?");
				if(!$verified){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$verified->bind_param('i', $storyNumDel);
				$verified->execute();
				$verified->close();
			}
		}
		if (isset($_POST['com'])) {//edit a comment
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}	
			$comNum = (!empty($_POST['comNum']) ? $_POST['comNum'] : null);
			$editCom = (!empty($_POST['editCom']) ? $_POST['editCom'] : null);
			$verified = $mysqli->prepare("select user_id from comments where id=?");
			if(!$verified){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$verified->bind_param('i', $comNum);
			$verified->execute();
			$verified->bind_result($user_id);
			while($verified->fetch()){
				printf("\t<li>%s",
					htmlspecialchars($user_id)
			);
			}
			$verified->close();
			if($user_id==$_SESSION['id']){
				$stmt = $mysqli->prepare("update comments set comment=? where id=?");
				if(!$stmt){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$stmt->bind_param('si', $editCom, $comNum);
				$stmt->execute();
				$stmt->close();
			}
		}

		if (isset($_POST['del2'])) {//delete a comment
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}	
			$comNumDel = (!empty($_POST['comNumDel']) ? $_POST['comNumDel'] : null); 
			$verified = $mysqli->prepare("select user_id from comments where id=?");
			if(!$verified){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$verified->bind_param('i', $comNumDel);
			$verified->execute();
			$verified->bind_result($user_id);
			while($verified->fetch()){
				printf("",
					htmlspecialchars($user_id)
			);
			}
			$verified->close();
			if($user_id==$_SESSION['id']){
				$comDelete = $mysqli->prepare("delete from comments where id=?");
				if(!$comDelete){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$comDelete->bind_param('i', $comNumDel);
				$comDelete->execute();
				$comDelete->close();
			}
		}

		if (isset($_POST['log'])){//logout code
			session_destroy();
		}
		$thing = $mysqli->prepare("select id, story, storyLink from stories where user_id=? order by id desc ");//to print stuff(working right now)
		if(!$thing){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$thing->bind_param('i', $_SESSION['id']);
		$thing->execute();
		 
		$result = $thing->get_result();

		echo "<table border='1'>
		<tr>
		<th>Story Number</th>
		<th>Story</th>
		<th>Story Link</th>
		</tr>";

		while($row = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>" . htmlspecialchars($row['id']) . "</td>";
			echo "<td>" . htmlspecialchars($row['story']) . "</td>";
			echo "<td>" . htmlspecialchars($row['storyLink'])."</td>";
			echo "</tr>";
		}

		echo "</table>";
			 
		$thing->close();

		$thing2 = $mysqli->prepare("select id, comment, story_id from comments where user_id=? order by id desc ");//to print stuff(working right now)
		if(!$thing2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$thing2->bind_param('i', $_SESSION['id']);
		$thing2->execute();
		 
		$result = $thing2->get_result();

		echo "<table border='1'>
		<tr>
		<th>Comment Number</th>
		<th>Comment</th>
		<th>Story Number the Comment is About</th>
		</tr>";

		while($row = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>" . htmlspecialchars($row['id']) . "</td>";
			echo "<td>" . htmlspecialchars($row['comment']) . "</td>";
			echo "<td>" . htmlspecialchars($row['story_id'])."</td>";
			echo "</tr>";
		}

		echo "</table>";
			 
		$thing2->close();
	?>
</body>
</html>
<?php 
require 'database.php';
session_start();
?>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>News Page</title>
</head>
<body>
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		<input type="submit" name = "con" value="Content Managment!" />
	</form>
	<form enctype="multipart/form-data" action = comments.php method="POST">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "com" value="To the Comments section!" />
	</form>
	<form enctype="multipart/form-data" action = userManager.php method="POST">
		<input type="submit" name = "usr" value="To User Management" />
	</form>
	<form enctype="multipart/form-data" action = newsPage.php method="POST">
		<input type="submit" name = "ref" value="Refresh!" />
	</form>
	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		<input type="submit" name = "log" value="Logout" />
	</form>
	<form enctype="multipart/form-data" action = newsPage.php method="POST">
		Story: <input type="text" name="story"><br>
		Link: <input type="text" name="linkToPage"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "sub" value="Submit!" />
	</form>

<?php
	require 'database.php';
	$id = $_SESSION['id'];
	if(!$_SESSION['token']){ //checks the token
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
	}


	if (isset($_POST['sub'])) {//Submit a story code
		if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}
		$story = $_POST['story'];
		$link = $_POST['linkToPage'];
		$stmt = $mysqli->prepare("insert into stories (story, storyLink, user_id) values (?, ?, ?)");
			if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('ssi', $story, $link, $id);
		$stmt->execute();
		$stmt->close();
	}

	if (isset($_POST['log'])){//logout code
		session_destroy();
	}
	$thing = $mysqli->prepare("select id, story, storyLink from stories order by id desc");//to print stuff(working right now)
	if(!$thing){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
 
	$thing->execute(); 
 
	$result = $thing->get_result();

	echo "<table border='1'>
	<tr>
	<th>Story Number</th>
	<th>Story</th>
	<th>Story Link</th>
	</tr>";

	while($row = mysqli_fetch_array($result)) {
	  echo "<tr>";
	  echo "<td>" . htmlspecialchars($row['id']) . "</td>";
	  echo "<td>" . htmlspecialchars($row['story']) . "</td>";
	  echo "<td>" . htmlspecialchars($row['storyLink'])."</td>";
	  echo "</tr>";
	}

	echo "</table>";
	 
	$thing->close();
	?>
</body>
</html>
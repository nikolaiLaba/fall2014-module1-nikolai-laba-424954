<?php 
require 'database.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>User Management</title>
</head>
<body>

	<form enctype="multipart/form-data" action = userManager.php method="POST">
		Change Username: <input type="text" name="newUser">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "user" value="Change!" />
	</form>

	<form enctype="multipart/form-data" action = userManager.php method="POST">
		Change Password: <input type="text" name="newPass">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "pass" value="Change!" />
	</form>

	<form enctype="multipart/form-data" action = userManager.php method="POST">
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "del" value="Delete Profile and all stories/comments you've posted" />
	</form>
	<form enctype="multipart/form-data" action = newsPage.php method="POST">
		<input type="submit" name = "ref" value="News Page!" />
	</form>


	<?php
		require 'database.php';
		if(!$_SESSION['token']){
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
		}	
		$id = (!empty($_SESSION['id']) ? $_SESSION['id'] : null);
		echo($_SESSION['id']);
		if(isset($_POST['pass'])){//code to change passwords
			$newPass = crypt($_POST['newPass'], "j?!!ododo9*oss");
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}
			$stmt = $mysqli->prepare("UPDATE users SET password=? WHERE id=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('si', $newPass, $id);
			$stmt->execute();
			$stmt->close();
		}
		if(isset($_POST['user'])){//code to change user names
			$newUser = (!empty($_POST['newUser']) ? $_POST['newUser'] : null);
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}
			$stmt = $mysqli->prepare("UPDATE users SET username=? WHERE id=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('si', $newUser, $id);
			$stmt->execute();
			$stmt->close();
		}
		if(isset($_POST['del'])){//code to wipe the entire user and all of his information
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}	
				$comDelete = $mysqli->prepare("delete from comments where user_id=?");
				if(!$comDelete){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$comDelete->bind_param('i', $_SESSION['id']);
				$comDelete->execute();
				$comDelete->close();
				$verified = $mysqli->prepare("delete from stories where user_id=?");
				if(!$verified){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$verified->bind_param('i', $_SESSION['id']);
				$verified->execute();
				$verified->close();
				$userDelete = $mysqli->prepare("delete from users where id=?");
				if(!$userDelete){ 
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$userDelete->bind_param('i', $_SESSION['id']);
				$userDelete->execute();
				$userDelete->close();

				header('Location: /~NikolaiLaba/loginPage.php');
		}
	?>
</body>
</html>
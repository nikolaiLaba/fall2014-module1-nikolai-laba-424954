<?php 
require 'database.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Comment Page</title>
</head>
<body>	
	<form enctype="multipart/form-data" action = comments.php method="POST">
		Story Number: <input type="text" name="storyNum">
		Comment: <input type="text" name="comment"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "com" value="Submit!" />
	</form>
	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		<input type="submit" name = "log" value="Logout" />
	</form>
	<form enctype="multipart/form-data" action = contentManager.php method="POST">
		<input type="submit" name = "con" value="Content Managment!" />
	</form>
	<form enctype="multipart/form-data" action = newsPage.php method="POST">
		<input type="submit" name = "ref" value="News Page!" />
	</form>
	<form enctype="multipart/form-data" action = comments.php method="POST">
		Search for Comments related to this Story Number: <input type="text" name="searchNum"><br>
		<input type ="hidden" name="token" value= "<?php echo $_SESSION['token']; ?>" />
		<input type="submit" name = "sub" value="Search!" />
	</form>
	Comments on this Story:
	<?php
		require 'database.php';
		$id = $_SESSION['id'];
		if(!$_SESSION['token']){
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
		}
		$postToken = $_SESSION['token'];
		if (isset($_POST['com'])) {//Post a comment
			if($_SESSION['token'] !== $_POST['token']){ //checks the token
				die("Request forgery detected");
			}
			$storyNum = (!empty($_POST['storyNum']) ? $_POST['storyNum'] : null);
			$comment = (!empty($_POST['comment']) ? $_POST['comment'] : null);
			$stmt = $mysqli->prepare("insert into comments (comment, story_id, user_id) values (?, ?, ?)");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('sii', $comment, $storyNum, $id);
			$stmt->execute();
			$stmt->close();
		}

		if (isset($_POST['log'])){//logout code
			session_destroy();
		}	
		if (isset($_POST['sub'])){//Search for comments relating to the story
			if($_SESSION['token'] !== $_POST['token']){
				die("Request forgery detected");
			}
			$thing = $mysqli->prepare("select id, comment, story_id from comments where story_id=? order by id desc");//to print stuff(working right now)
			if(!$thing){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
		}
	 	$thing->bind_param('i', $_POST['searchNum']);
		$thing->execute();
		 
		$result = $thing->get_result();

		echo "<table border='1'>
		<tr>
		<th>Comment Number</th>
		<th>Comment</th>
		<th>Story Number</th>
		</tr>";
		while($row = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>" . htmlspecialchars($row['id']) . "</td>";
			echo "<td>" . htmlspecialchars($row['comment']) . "</td>";
			echo "<td>" . htmlspecialchars($row['story_id'])."</td>";
			echo "</tr>";
		}

		echo "</table>"; 
		$thing->close();
		exit;
		}
	?>
</body>
</html>
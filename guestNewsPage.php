<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Guest News</title>
</head>
<body>	
	<form enctype="multipart/form-data" action = loginPage.php method="POST">
		<input type="submit" name = "ref" value="Login or Register!" />
	</form>
	<?php
		require 'database.php';
		$thing = $mysqli->prepare("select id, story, storyLink from stories order by id desc");//print stories
		if(!$thing){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$thing->execute();
		$result = $thing->get_result();
		echo "<table border='1'>
		<tr>
		<th>Story Number</th>
		<th>Story</th>
		<th>Story Link</th>
		</tr>";

		while($row = mysqli_fetch_array($result)) {
		  echo "<tr>";
		  echo "<td>" . htmlspecialchars($row['id']) . "</td>";
		  echo "<td>" . htmlspecialchars($row['story']) . "</td>";
		  echo "<td>" . htmlspecialchars($row['storyLink'])."</td>";
		  echo "</tr>";
		}

		echo "</table>";
		 
		$thing->close();

		$thing2 = $mysqli->prepare("select id, comment, story_id from comments order by id desc");//print comments
		if(!$thing2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
	 
		$thing2->execute();
	 
		$result = $thing2->get_result();

		echo "<table border='1'>
		<tr>
		<th>Comment Number</th>
		<th>Comment</th>
		<th>Story Number this Comment is About</th>
		</tr>";

		while($row = mysqli_fetch_array($result)) {
		  echo "<tr>";
		  echo "<td>" . htmlspecialchars($row['id']) . "</td>";
		  echo "<td>" . htmlspecialchars($row['comment']) . "</td>";
		  echo "<td>" . htmlspecialchars($row['story_id'])."</td>";
		  echo "</tr>";
		}
		echo "</table>";
		 
		$thing2->close();
	?>
</body>
</html>